<?php

//use Zend\Config\Reader\Ini;
//
//$config = (new Ini())->fromFile(APP_PASSWD);
return [
    'doctrine' => [
        'connection' => [
            'orm_animals' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOPgSql\Driver',
                'params' => [
                    'port' => '5432',
                    'host' => '127.0.0.1',
                    'user' => 'postgres',
                    'password' => '123456',
                    'dbname' => 'db_zeus',
                    'schema' => 'Animals',
                    'charset' => 'utf8',
                ],
            ],
            'orm_migration' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOPgSql\Driver',
                'params' => [
                    'port' => '5432',
                    'host' => '127.0.0.1',
                    'user' => 'postgres',
                    'password' => '123456',
                    'dbname' => 'db_zeus',
                    'schema' => 'animals',
                    'charset' => 'utf8',
                ]
            ],
        ],
    ],
];
