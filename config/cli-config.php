<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

// replace with file to your own project bootstrap
$arquiv = require_once __DIR__ . '/../test/bootstrap.php';

use Jonico\Test\Bootstrap;

// replace with mechanism to retrieve EntityManager in your app
$entityManager = Bootstrap::getServiceManager()->get('doctrine.entitymanager.orm_migration');

return ConsoleRunner::createHelperSet($entityManager);
