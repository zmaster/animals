<?php

//use Zend\Config\Reader\Ini;
//
//$config = (new Ini())->fromFile(APP_PASSWD);

return [
    'doctrine' => [
        'entitymanager' => [
            'orm_animals' => [
                'connection' => 'orm_animals',
                'configuration' => 'orm_animals'
            ],
            'orm_migration' => [
                'connection' => 'orm_migration',
                'configuration' => 'orm_animals'
            ],
        ],
        'connection' => [
            'orm_animals' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOPgSql\Driver',
                'params' => [
                    'port' => '5432',
                    'host' => '127.0.0.1',
                    'user' => 'postgres',
                    'password' => '123456',
                    'dbname' => 'db_zeus',
                    'schema' => 'Animals',
                    'charset' => 'utf8',
                ],
            ],
            'orm_migration' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOPgSql\Driver',
                'params' => [
                    'port' => '5432',
                    'host' => '127.0.0.1',
                    'user' => 'postgres',
                    'password' => '123456',
                    'dbname' => 'db_zeus',
                    'schema' => 'animals',
                    'charset' => 'utf8',
                ]
            ],
        ],
        'configuration' => [
            'orm_animals' => [
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'driver' => 'orm_animals',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
                'filters' => []
            ],
        ],
        'driver' => [
            'orm_animals' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../../../module/Animals/src/Animals/V1/Rest/Animal',
                ]
            ],
        ],
    ]
];
