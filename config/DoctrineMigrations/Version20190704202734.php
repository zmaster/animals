<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190704202734 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE animals.animal
			(
			  id_animal serial NOT NULL,
			  id_owner integer NOT NULL,
			  st_name character varying(150) NOT NULL,
			  bo_alive boolean NOT NULL,
			  st_pedigree character varying(150),
			  st_chip character varying(150),
			  id_specie integer,
			  id_breed integer,
			  id_postage integer,
			  nu_weight numeric,
			  id_sexo integer, -- 0 - macho...
			  bo_castrated boolean,
			  st_color character(60),
			  st_coat character(60),
			  dt_birth date,
			  CONSTRAINT pk_id_animal PRIMARY KEY (id_animal)
			)
			WITH (
			  OIDS=FALSE
			);'
		);
		
		$this->addSql('ALTER TABLE animals.animal
			OWNER TO postgres;');

		$this->addSql("COMMENT ON COLUMN animals.animal.id_sexo IS '0 - macho
			1- femea
			3- nao defido';"
		);


    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE animals.animal;');

    }
}
