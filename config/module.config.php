<?php
return array(
    'service_manager' => array(
        'factories' => array(
            'Animals\\V1\\Rest\\Animal\\AnimalResource' => 'Animals\\V1\\Rest\\Animal\\AnimalResourceFactory',
            'Animals\\V1\\Rest\\Specie\\SpecieResource' => 'Animals\\V1\\Rest\\Specie\\SpecieResourceFactory',
            'Animals\\V1\\Rest\\Breed\\BreedResource' => 'Animals\\V1\\Rest\\Breed\\BreedResourceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'animals.rest.animal' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/animal[/:id_animal]',
                    'defaults' => array(
                        'controller' => 'Animals\\V1\\Rest\\Animal\\Controller',
                    ),
                ),
            ),
            'animals.rest.specie' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/specie[/:id]',
                    'defaults' => array(
                        'controller' => 'Animals\\V1\\Rest\\Specie\\Controller',
                    ),
                ),
            ),
            'animals.rest.breed' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/breed[/:id]',
                    'defaults' => array(
                        'controller' => 'Animals\\V1\\Rest\\Breed\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'animals.rest.animal',
            1 => 'animals.rest.specie',
            2 => 'animals.rest.breed',
        ),
    ),
    'zf-rest' => array(
        'Animals\\V1\\Rest\\Animal\\Controller' => array(
            'listener' => 'Animals\\V1\\Rest\\Animal\\AnimalResource',
            'route_name' => 'animals.rest.animal',
            'route_identifier_name' => 'id_animal',
            'collection_name' => 'animal',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Animals\\V1\\Rest\\Animal\\AnimalEntity',
            'collection_class' => 'Animals\\V1\\Rest\\Animal\\AnimalCollection',
            'service_name' => 'Animal',
        ),
        'Animals\\V1\\Rest\\Specie\\Controller' => array(
            'listener' => 'Animals\\V1\\Rest\\Specie\\SpecieResource',
            'route_name' => 'animals.rest.specie',
            'route_identifier_name' => 'id',
            'collection_name' => 'specie',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Animals\\V1\\Rest\\Specie\\SpecieEntity',
            'collection_class' => 'Animals\\V1\\Rest\\Specie\\SpecieCollection',
            'service_name' => 'specie',
        ),
        'Animals\\V1\\Rest\\Breed\\Controller' => array(
            'listener' => 'Animals\\V1\\Rest\\Breed\\BreedResource',
            'route_name' => 'animals.rest.breed',
            'route_identifier_name' => 'id',
            'collection_name' => 'breed',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'POST',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(
                0 => 'idSpecie',
            ),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'Animals\\V1\\Rest\\Breed\\BreedEntity',
            'collection_class' => 'Animals\\V1\\Rest\\Breed\\BreedCollection',
            'service_name' => 'breed',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Animals\\V1\\Rest\\Animal\\Controller' => 'HalJson',
            'Animals\\V1\\Rest\\Specie\\Controller' => 'HalJson',
            'Animals\\V1\\Rest\\Breed\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'Animals\\V1\\Rest\\Animal\\Controller' => array(
                0 => 'application/vnd.animals.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Animals\\V1\\Rest\\Specie\\Controller' => array(
                0 => 'application/vnd.animals.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Animals\\V1\\Rest\\Breed\\Controller' => array(
                0 => 'application/vnd.animals.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'Animals\\V1\\Rest\\Animal\\Controller' => array(
                0 => 'application/vnd.animals.v1+json',
                1 => 'application/json',
            ),
            'Animals\\V1\\Rest\\Specie\\Controller' => array(
                0 => 'application/vnd.animals.v1+json',
                1 => 'application/json',
            ),
            'Animals\\V1\\Rest\\Breed\\Controller' => array(
                0 => 'application/vnd.animals.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'Animals\\V1\\Rest\\Animal\\AnimalEntity' => array(
                'entity_identifier_name' => 'idAnimal',
                'route_name' => 'animals.rest.animal',
                'route_identifier_name' => 'id_animal',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'Animals\\V1\\Rest\\Animal\\AnimalCollection' => array(
                'entity_identifier_name' => 'idAnimal',
                'route_name' => 'animals.rest.animal',
                'route_identifier_name' => 'id_animal',
                'is_collection' => true,
            ),
            'Animals\\V1\\Rest\\Specie\\SpecieEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animals.rest.specie',
                'route_identifier_name' => 'id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'Animals\\V1\\Rest\\Specie\\SpecieCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animals.rest.specie',
                'route_identifier_name' => 'id',
                'is_collection' => true,
            ),
            'Animals\\V1\\Rest\\Breed\\BreedEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animals.rest.breed',
                'route_identifier_name' => 'id',
                'hydrator' => 'DoctrineModule\\Stdlib\\Hydrator\\DoctrineObject',
            ),
            'Animals\\V1\\Rest\\Breed\\BreedCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'animals.rest.breed',
                'route_identifier_name' => 'id',
                'is_collection' => true,
            ),
        ),
    ),
);
