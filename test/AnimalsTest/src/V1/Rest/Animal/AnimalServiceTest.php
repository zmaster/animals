<?php

namespace OversightTest\V1\Rest\Account;

use PHPUnit\Framework\TestCase;
use Jonico\Test\Bootstrap;
use Zend\Stdlib\Parameters;
use Oversight\V1\Rest\Account\AccountService;
use Oversight\V1\Rest\Plan\PlanService;
use Oversight\V1\Rest\Profile\ProfileService;
use Oversight\V1\Rest\Collaborator\CollaboratorService;

/**
 * @group account
 */
class AccountServiceTest extends TestCase
{

    /**
     * @var \Oversight\V1\Rest\Account\AccountService
     */
    private $service;
    /**
     *
     * @var CollaboratorService
     */
    private $collaboratorService;
    private $em;

    public function __construct()
    {
        $this->em = Bootstrap::getServiceManager()->get('doctrine.entitymanager.orm_oversight');
        $this->service = Bootstrap::getServiceManager()->get(AccountService::SERVICE_CLASS);
        $this->planService = Bootstrap::getServiceManager()->get(PlanService::SERVICE_CLASS);
        $this->profileService = Bootstrap::getServiceManager()->get(ProfileService::SERVICE_CLASS);
        $this->collaboratorService = Bootstrap::getServiceManager()->get(CollaboratorService::SERVICE_CLASS);
    }

    /**
     * @function setUp
     * Função à ser executada na inicialização do teste unitário
     */
    public function setUp()
    {
        $this->em->getConnection()->beginTransaction();
    }

    /**
     * @function tearDown
     * Função à ser utilizada na finalização do teste unitário
     */
    public function tearDown()
    {
        $this->em->getConnection()->rollback();
    }

    /**
     *
     * @return \Oversight\V1\Rest\Plan\PlanEntity
     */
    protected function createPlan()
    {
        $objParamsProfile = new Parameters([
            'name' => 'Teste unitario perfil',
            'description' => 'Descrição do perfil',
            'status' => true
        ]);

        $objProfile = $this->profileService->create($objParamsProfile);
        $objParamsProfile2 = new Parameters([
            'name' => 'Teste unitario perfil 2',
            'description' => 'Descrição do perfil 2',
            'status' => true
        ]);

        $objProfile2 = $this->profileService->create($objParamsProfile2);

        $objParams = new Parameters([
            'profiles' => [$objProfile->getId(), $objProfile2->getId()],
            'name' => 'Basic teste',
            'description' => 'Plano basico de teste'
        ]);

        return $this->planService->create($objParams);
    }

    /**
     *
     * @return \Oversight\V1\Rest\Account\AccountEntity
     */
    protected function create()
    {
        $objPlan = $this->createPlan();
        $objParams = new Parameters([
            'name' => 'Empresa teste',
            'socialName' => 'Nome fantasia da empresa',
            'cnpj' => '50978320000199',
            'inscriptionState' => '132154654113',
            'inscriptionCity' => '',
            'createRegister' => '2009-12-05',
            'mail' => 'contact@empresa.com.br',
            'userName' => 'userTeste',
            'plan' => $objPlan->getId()
        ]);
        return $this->service->create($objParams);
    }

    /**
     * @group account-create
     */
    public function testCreate()
    {
        $objAccount = $this->create();
        $this->assertNotEmpty($objAccount->getId());
    }

    /**
     * @group account-create-cnpj-exists
     * @expectedException \Jonico\Exception\ValidateException
     */
    public function testCreateCnpjExistsException()
    {
        $this->create();
        $objParams = new Parameters([
            'name' => 'Empresa teste',
            'socialName' => 'Nome fantasia da empresa',
            'cnpj' => '50978320000199',
            'inscriptionState' => '132154654113',
            'inscriptionCity' => '',
            'createRegister' => '2009-12-05',
            'mail' => 'contact2@empresa.com.br',
            'plan' => 1
        ]);
        $this->service->create($objParams);
    }

    /**
     * @group account-create-name-empty
     * @expectedException Jonico\Exception\ValidateException
     */
    public function testCreateNameEmptyException()
    {
        $objParams = new Parameters([
            'name' => '',
            'socialName' => 'Nome fantasia da empresa',
            'cnpj' => '50978320000199',
            'inscriptionState' => '132154654113',
            'inscriptionCity' => '',
            'createRegister' => '2009-12-05',
            'mail' => 'contact@empresa.com.br',
            'plan' => 1
        ]);
        $this->service->create($objParams);
    }

    /**
     * @group account-create-plan-empty
     * @expectedException Jonico\Exception\ValidateException
     */
    public function testCreatePlanEmptyException()
    {
        $objParams = new Parameters([
            'name' => '',
            'socialName' => 'Nome fantasia da empresa',
            'cnpj' => '50978320000199',
            'inscriptionState' => '132154654113',
            'inscriptionCity' => '',
            'createRegister' => '2009-12-05',
            'mail' => 'contact@empresa.com.br',
            'plan' => ''
        ]);
        $this->service->create($objParams);
    }

    /**
     * @group account-getall
     */
    public function testGetAll()
    {
        $this->create();
        $arrAccount = $this->service->getAll([]);
        $this->assertNotEmpty($arrAccount);
    }

    /**
     * @group account-getall
     */
    public function testGetAllForName()
    {
        $objAccount = $this->create();
        $arrAccount = $this->service->getAll(['name' => $objAccount->getName()]);
        $this->assertNotEmpty($arrAccount);
        $this->assertEquals($objAccount->getName(), $arrAccount[0]->getName());
    }

    /**
     * @group account-getbyid
     */
    public function testById()
    {
        $objAccount = $this->create();
        $objAccountGetById = $this->service->getById($objAccount->getId());
        $this->assertEquals($objAccount->getName(), $objAccountGetById->getName());
        $this->assertEquals($objAccount->getSocialName(), $objAccountGetById->getSocialName());
        $this->assertEquals($objAccount->getCnpj(), $objAccountGetById->getCnpj());
        $this->assertEquals($objAccount->getInscriptionState(), $objAccountGetById->getInscriptionState());
        $this->assertEquals($objAccount->getInscriptionCity(), $objAccountGetById->getInscriptionCity());
        $this->assertEquals($objAccount->getCreateRegister()->format('Y-m-d'), $objAccountGetById->getCreateRegister()->format('Y-m-d'));
        $this->assertEquals($objAccount->getMail(), $objAccountGetById->getMail());
    }

    /**
     * @expectedException \Jonico\Exception\ValidateException
     */
    public function testGetByIdNotExist()
    {
        $this->service->getById(234987384);
    }

    /**
     * @group account-edit
     */
    public function testEdit()
    {
        $objAccount = $this->create();
        $objParams = new Parameters([
            'name' => 'Empresa de teste 2',
            'socialName' => 'Nome fantasia da empresa 2',
            'inscriptionState' => '987654321',
            'inscriptionCity' => '',
            'mail' => 'contact2@empresa.com.br'
        ]);
        $objAccountEdit = $this->service->edit($objAccount->getId(), $objParams);
        $this->assertEquals($objParams->name, $objAccountEdit->getName());
        $this->assertEquals($objParams->socialName, $objAccountEdit->getSocialName());
        $this->assertEquals($objParams->inscriptionState, $objAccountEdit->getInscriptionState());
        $this->assertEquals($objParams->inscriptionCity, $objAccountEdit->getInscriptionCity());
        $this->assertEquals($objParams->mail, $objAccountEdit->getMail());
    }

    /**
     * @group account-edit-not-inscription-state
     * @expectedException \Jonico\Exception\ValidateException
     */
    public function testEditNotInscriptionStateException()
    {
        $objAccount = $this->create();
        $objParams = new Parameters([
            'name' => 'Empresa de teste 2',
            'socialName' => 'Nome fantasia da empresa 2',
            'inscriptionState' => '',
            'inscriptionCity' => '',
            'mail' => 'contact2@empresa.com.br'
        ]);
        $this->service->edit($objAccount->getId(), $objParams);
    }

    /**
     * @group account-edit-mail-exists
     * @expectedException \Jonico\Exception\ValidateException
     */
    public function testEditMailExistsException()
    {
        $objPlan = $this->createPlan();
        $objParams1 = new Parameters([
            'name' => 'Empresa teste mail',
            'socialName' => 'Nome fantasia da empresa mail',
            'cnpj' => '12345678998765',
            'inscriptionState' => '132154623413',
            'inscriptionCity' => '',
            'createRegister' => '2009-12-10',
            'mail' => 'contact2@empresa.com.br',
            'plan' => $objPlan->getId()
        ]);
        $this->service->create($objParams1);
        $objAccount = $this->create();
        $objParams = new Parameters([
            'name' => 'Empresa de teste 2',
            'socialName' => 'Nome fantasia da empresa 2',
            'inscriptionState' => '987654321',
            'inscriptionCity' => '',
            'mail' => 'contact2@empresa.com.br'
        ]);
        $objAccountEdit = $this->service->edit($objAccount->getId(), $objParams);
        $this->assertEquals($objParams->name, $objAccountEdit->getName());
        $this->assertEquals($objParams->socialName, $objAccountEdit->getSocialName());
        $this->assertEquals($objParams->inscriptionState, $objAccountEdit->getInscriptionState());
        $this->assertEquals($objParams->inscriptionCity, $objAccountEdit->getInscriptionCity());
        $this->assertEquals($objParams->mail, $objAccountEdit->getMail());
    }

    /**
     * @group account-delete
     */
    public function testDelete()
    {
        $objAccount = $this->create();
        $this->service->delete($objAccount->getId());
        $objAccountRemove = $this->service->getById($objAccount->getId());
        $this->assertEquals(false, $objAccountRemove->getStatus());
    }

    /**
     * @group account-activate
     */
    public function testActivate()
    {
        $objAccount = $this->create();
        $objParamsCollaborator = new Parameters([
            'name' => 'Jhoe Doe',
            'email' => 'jhoedoe@email.com',
            'cpf' => '30010020099',
            'rg' => '2463399',
            'dtCreate' => '2017-03-25',
            'dtBirth' => '1982-01-01',
            'inActive' => true,
            'rgOrganExpedition' => 'SSP-DF',
            'dtExpedition' => '2008-10-09',
            'naturalness' => 'Brasileiro',
            'city' => 'Taguatinga',
            'account' => $objAccount->getId(),
            'administrator' => true,
            'userName' => 'colaboradorteste',
            'contact' => false
        ]);
        $this->collaboratorService->create($objParamsCollaborator);
        $objParams = $this->service->activate($objAccount->getId());
        $this->assertNotEmpty($objParams->password);
        $objAccountActive = $this->service->getById($objAccount->getId());
        $this->assertTrue($objAccountActive->getStatus());
    }

    /**
     * @group account-activate
     * @expectedException \Jonico\Exception\ValidateException
     */
    public function testActivateEmptyCollaborator()
    {
        $objAccount = $this->create();
        $objParams = $this->service->activate($objAccount->getId());
        $this->assertNotEmpty($objParams->password);
        $objAccountActive = $this->service->getById($objAccount->getId());
        $this->assertTrue($objAccountActive->getStatus());
    }

}
