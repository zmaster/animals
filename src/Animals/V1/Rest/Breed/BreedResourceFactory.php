<?php
namespace Animals\V1\Rest\Breed;

class BreedResourceFactory
{
    public function __invoke($services)
    {
        return new BreedResource($services);
    }
}
