<?php

namespace Animals\V1\Rest\Breed;

use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of AccountServiceFactory
 */
class BreedServiceFactory
{

    public function __invoke(ServiceLocatorInterface $objServiceLocator)
    {
        $objEntityManager = $objServiceLocator->get('doctrine.entitymanager.orm_animals');
        $objService = new BreedService($objEntityManager);
        $objService->setServiceManager($objServiceLocator);
        return $objService;
    }

}
