<?php
namespace Animals\V1\Rest\Breed;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BreedEntity
 * @package Animals\V1\Rest\Breed
 * @ORM\Table(name="animals.breed")
 * @ORM\Entity(repositoryClass="BreedRepository")
 */

class BreedEntity
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="animals.animal_id_animal_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="id_specie", type="integer", nullable=false)
     */
    private $idSpecie;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return integer
     */
    public function getIdSpecie()
    {
        return $this->idSpecie;
    }

    /**
     * @param integer $idSpecie
     *
     * @return self
     */
    public function setIdSpecie($idSpecie)
    {
        $this->idSpecie = $idSpecie;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
