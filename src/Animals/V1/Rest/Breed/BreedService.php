<?php
namespace Animals\V1\Rest\Breed;

use Jonico\Service\AbstractService;

class BreedService extends AbstractService
{
    public function getAll($objParams)
    {
        return $this->em->getRepository(BreedEntity::class)->findBy((array) $objParams);
    }

    public function create($data)
    {
        $objBreed = new BreedEntity();
        $objBreed->setName($data->name);
        $objBreed->setIdSpecie($data->idSpecie);
        parent::persist($objBreed);
        return $objBreed;
    }
}