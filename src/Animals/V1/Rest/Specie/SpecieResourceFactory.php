<?php
namespace Animals\V1\Rest\Specie;

class SpecieResourceFactory
{
    public function __invoke($services)
    {
        return new SpecieResource($services);
    }
}
