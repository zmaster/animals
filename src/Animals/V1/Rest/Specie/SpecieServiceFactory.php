<?php

namespace Animals\V1\Rest\Specie;

use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of AccountServiceFactory
 */
class SpecieServiceFactory
{

    public function __invoke(ServiceLocatorInterface $objServiceLocator)
    {
        $objEntityManager = $objServiceLocator->get('doctrine.entitymanager.orm_animals');
        $objService = new SpecieService($objEntityManager);
        $objService->setServiceManager($objServiceLocator);
        return $objService;
    }

}
