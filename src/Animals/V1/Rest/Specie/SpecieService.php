<?php
namespace Animals\V1\Rest\Specie;

use Jonico\Service\AbstractService;

class SpecieService extends AbstractService
{
    public function getAll($objParams)
    {
        return $this->em->getRepository(SpecieEntity::class)->findBy((array) $objParams);
    }

}