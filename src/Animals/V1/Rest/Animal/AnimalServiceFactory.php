<?php

namespace Animals\V1\Rest\Animal;

use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of AccountServiceFactory
 */
class AnimalServiceFactory
{

    public function __invoke(ServiceLocatorInterface $objServiceLocator)
    {
        $objEntityManager = $objServiceLocator->get('doctrine.entitymanager.orm_animals');
        $objService = new AnimalService($objEntityManager);
        $objService->setServiceManager($objServiceLocator);
        return $objService;
    }

}
