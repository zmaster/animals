<?php
namespace Animals\V1\Rest\Animal;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;

use Zend\Paginator\Paginator;

class AnimalCollection extends Paginator
{
	public function __construct($objProfile)
    {
        parent::__construct(new ArrayAdapter($objProfile));
    }
}
