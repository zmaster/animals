<?php
namespace Animals\V1\Rest\Animal;
use Doctrine\ORM\Mapping as ORM;
/**
 * Class AnimalEntity
 * @package Animals\V1\Rest\Animal
 * @ORM\Table(name="animals.animal")
 * @ORM\Entity(repositoryClass="AnimalRepository")
 */

class AnimalEntity
{
	/**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id_animal", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="animals.animal_id_animal_seq", allocationSize=1, initialValue=1)
     */
    private $idAnimal;

    /**
     * @var integer
     * @ORM\Column(name="id_owner", type="integer", nullable=false)
     */
    private $idOwner;

    /**
     * @var string
     * @ORM\Column(name="st_name", type="string", nullable=false)
     */
    private $stName;

    /**
     * @var bool
     * @ORM\Column(name="bo_alive", type="boolean", nullable=false)
     */
    private $boAlive;

    /**
     * @var string
     * @ORM\Column(name="st_pedigree", type="string", nullable=true)
     */
    private $stPedigree;

    /**
     * @var string
     * @ORM\Column(name="st_chip", type="string", nullable=true)
     */
    private $stChip;

    /**
     * @var integer
     * @ORM\Column(name="id_specie", type="integer", nullable=true)
     */
    private $idSpecie;

    /**
     * @var integer
     * @ORM\Column(name="id_breed", type="integer", nullable=true)
     */
    private $idBreed;

    /**
     * @var string
     * @ORM\Column(name="st_postage", type="string", nullable=true)
     */
    private $stPostage;

    /**
     * @var float
     * @ORM\Column(name="nu_weight", type="float", nullable=true)
     */
    private $nuWeight;

    /**
     * @var integer
     * @ORM\Column(name="id_sexo", type="integer", nullable=true)
     */
    private $idSexo;

    /**
     * @var bool
     * @ORM\Column(name="bo_castrated", type="boolean", nullable=true)
     */
    private $boCastrated;

    /**
     * @var string
     * @ORM\Column(name="st_color", type="string", nullable=true)
     */
    private $stColor;

    /**
     * @var string
     * @ORM\Column(name="st_coat", type="string", nullable=true)
     */
    private $stCoat;

    /**
     * @var string
     * @ORM\Column(name="dt_birth", type="string", nullable=true)
     */
    private $dtBirth;

    /**
     * @return integer
     */
    public function getIdAnimal()
    {
        return $this->idAnimal;
    }

    /**
     * @param integer $idAnimal
     *
     * @return self
     */
    public function setIdAnimal($idAnimal)
    {
        $this->idAnimal = $idAnimal;

        return $this;
    }

    /**
     * @return integer
     */
    public function getIdOwner()
    {
        return $this->idOwner;
    }

    /**
     * @param integer $idOwner
     *
     * @return self
     */
    public function setIdOwner($idOwner)
    {
        $this->idOwner = $idOwner;

        return $this;
    }

    /**
     * @return string
     */
    public function getStName()
    {
        return $this->stName;
    }

    /**
     * @param string $stName
     *
     * @return self
     */
    public function setStName($stName)
    {
        $this->stName = $stName;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBoAlive()
    {
        return $this->boAlive;
    }

    /**
     * @param bool $boAlive
     *
     * @return self
     */
    public function setBoAlive($boAlive)
    {
        $this->boAlive = $boAlive;

        return $this;
    }

    /**
     * @return string
     */
    public function getStPedigree()
    {
        return $this->stPedigree;
    }

    /**
     * @param string $stPedigree
     *
     * @return self
     */
    public function setStPedigree($stPedigree)
    {
        $this->stPedigree = $stPedigree;

        return $this;
    }

    /**
     * @return string
     */
    public function getStChip()
    {
        return $this->stChip;
    }

    /**
     * @param string $stChip
     *
     * @return self
     */
    public function setStChip($stChip)
    {
        $this->stChip = $stChip;

        return $this;
    }

    /**
     * @return integer
     */
    public function getIdSpecie()
    {
        return $this->idSpecie;
    }

    /**
     * @param integer $idSpecie
     *
     * @return self
     */
    public function setIdSpecie($idSpecie)
    {
        $this->idSpecie = $idSpecie;

        return $this;
    }

    /**
     * @return integer
     */
    public function getIdBreed()
    {
        return $this->idBreed;
    }

    /**
     * @param integer $idBreed
     *
     * @return self
     */
    public function setIdBreed($idBreed)
    {
        $this->idBreed = $idBreed;

        return $this;
    }

    /**
     * @return string
     */
    public function getStPostage()
    {
        return $this->stPostage;
    }

    /**
     * @param string $stPostage
     */
    public function setStPostage($stPostage)
    {
        $this->stPostage = $stPostage;

        return $this;
    }


    /**
     * @return double
     */
    public function getNuWeight()
    {
        return $this->nuWeight;
    }

    /**
     * @param double $nuWeight
     *
     * @return self
     */
    public function setNuWeight($nuWeight)
    {
        $this->nuWeight = $nuWeight;

        return $this;
    }

    /**
     * @return integer
     */
    public function getIdSexo()
    {
        return $this->idSexo;
    }

    /**
     * @param integer $idSexo
     *
     * @return self
     */
    public function setIdSexo($idSexo)
    {
        $this->idSexo = $idSexo;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBoCastrated()
    {
        return $this->boCastrated;
    }

    /**
     * @param bool $boCastrated
     *
     * @return self
     */
    public function setBoCastrated($boCastrated)
    {
        $this->boCastrated = $boCastrated;

        return $this;
    }

    /**
     * @return string
     */
    public function getStColor()
    {
        return $this->stColor;
    }

    /**
     * @param string $stColor
     *
     * @return self
     */
    public function setStColor($stColor)
    {
        $this->stColor = $stColor;

        return $this;
    }

    /**
     * @return string
     */
    public function getStCoat()
    {
        return $this->stCoat;
    }

    /**
     * @param string $stCoat
     *
     * @return self
     */
    public function setStCoat($stCoat)
    {
        $this->stCoat = $stCoat;

        return $this;
    }

    /**
     * @return string
     */
    public function getDtBirth()
    {
        return $this->dtBirth;
    }

    /**
     * @param string $dtBirth
     *
     * @return self
     */
    public function setDtBirth($dtBirth)
    {
        $this->dtBirth = $dtBirth;

        return $this;
    }
}
