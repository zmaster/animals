<?php
namespace Animals\V1\Rest\Animal;

use Jonico\Service\AbstractService;

class AnimalService extends AbstractService
{
    public function registerAnimal($params = null)
    {
        $objAnimal = new AnimalEntity();
        $objAnimal->setIdOwner($params->owner);
        $objAnimal->setStName($params->name);
        $objAnimal->setBoAlive($params->alive);
        $objAnimal->setStPedigree($params->pedigree);
        $objAnimal->setStChip($params->chip);
        $objAnimal->setIdSpecie($params->specie);
        $objAnimal->setIdBreed($params->breed);
        $objAnimal->setStPostage($params->postage);
        $objAnimal->setNuWeight(floatval(str_replace(',', '.', $params->weight)));
        $objAnimal->setIdSexo($params->sexo);
        $objAnimal->setBoCastrated($params->castrated);
        $objAnimal->setStColor($params->color);
        $objAnimal->setStCoat($params->coat);
        $objAnimal->setDtBirth($params->birth);
        parent::persist($objAnimal);
        return $objAnimal;
    }

    public function getAllAnimals()
    {
        $objQueryBuilder = $this->em->createQueryBuilder()->select('animals.animal')->from(AnimalEntity::class, 'animal')->getQuery()->getResult();
    }
}