<?php
namespace Animals;

use ZF\Apigility\Provider\ApigilityProviderInterface;

class Module implements ApigilityProviderInterface
{
    public function getConfig()
    {
        $arrConfig = array_replace_recursive(
                        include __DIR__ . '/../../config/global.php',include __DIR__ . '/../../config/module.config.php'
                );
        return $arrConfig;
    }
    

    public function getAutoloaderConfig()
    {
        return [
            'ZF\Apigility\Autoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__,
                ],
            ],
        ];
    }



    public function getServiceConfig()
    {
        return [
            'factories' => [
                \Animals\V1\Rest\Animal\AnimalService::class => V1\Rest\Animal\AnimalServiceFactory::class,
                \Animals\V1\Rest\Breed\BreedService::class => V1\Rest\Breed\BreedServiceFactory::class,
                \Animals\V1\Rest\Specie\SpecieService::class => V1\Rest\Specie\SpecieServiceFactory::class,
            ]
        ];
    }
}
